<?php

namespace Drupal\config_token\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Displays the Config Token settings form.
 */
class ConfigTokensForm extends ConfigFormBase {

  /**
   * Module Handler service object.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor for Config Tokens.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module Handler service object.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config_token.tokens'];
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormId() {
    return 'config_tokens';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get allowed tokens.
    $allowed_tokens = $this->config('config_token.settings')->get('allowed_tokens');
    // Populate select options.
    $allowed_tokens_options = [];
    foreach ($allowed_tokens as $token => $details) {
      $allowed_tokens_options[$token] = $details['name'];
    }
    asort($allowed_tokens_options);

    // Get config.
    $config = $this->config('config_token.tokens');
    $replacements = (array) $config->get('replacements');
    // Add empty entry at the end of list.
    $replacements[''] = '';

    $form['values'] = [
      '#type' => 'table',
      '#caption' => '',
      '#header' => [
        $this->t('Token'),
        $this->t('Value'),
        $this->t('Action'),
      ],
    ];

    $i = 0;
    foreach ($replacements as $token => $value) {
      $i++;

      // Get details about allowed tokens.
      if (!empty($allowed_tokens[$token])) {
        $details = $allowed_tokens[$token];
      }

      $form['values'][$i]['token'] = [
        '#type' => 'select',
        '#title' => ($details['name']) ? $details['name'] . '. Usage: [config_token:' . $token . ']' : '',
        '#options' => $allowed_tokens_options,
        '#value' => $token,
      ];

      $form['values'][$i]['value'] = [
        '#type' => 'textfield',
        '#title' => ($details['name']) ? $details['format_id'] : '',
        '#value' => $value,
      ];

      if (!empty($value)) {
        $form['values'][$i]['delete'] = [
          '#type' => 'submit',
          '#value' => $this->t('Delete'),
          '#name' => 'delete',
          '#index' => $i,
        ];
      }
      else {
        $form['values'][$i]['add'] = [
          '#type' => 'submit',
          '#value' => $this->t('Add'),
          '#name' => 'add',
          '#index' => $i,
        ];
      }
    }

    if ($this->moduleHandler->moduleExists('help')) {
      $markup = '<p>' . $this->t('See the list of <a href=":field#token-config-token" target="_blank">available tokens</a>. For more information, see the <a href=":docs" target="_blank">online documentation.</a></p>',
        [
          ':field' => Url::fromRoute('help.page', ['name' => 'token'])->toString(),
          ':docs' => Url::fromRoute('help.page', ['name' => 'config_token'])->toString(),
        ]
      );

      $form['values'][]['help'] = [
        '#markup' => $markup,
        '#wrapper_attributes' => [
          'colspan' => 2,
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('config_token.tokens');
    $trigger = $form_state->getTriggeringElement();
    $values = $form_state->getValue('values');
    $replacements = [];
    foreach ($values as $key => $item) {
      // Delete button was used.
      if ($trigger['#name'] === 'delete' && $key === $trigger['#index']) {
        continue;
      }
      // Delete empty values.
      if (empty($item) || empty($item['value'])) {
        continue;
      }
      $replacements[$item['token']] = $item['value'];
    }
    $config->set('replacements', $replacements);
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
