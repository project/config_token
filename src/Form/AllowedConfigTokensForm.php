<?php

namespace Drupal\config_token\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Displays the Allowed Tokens form.
 */
class AllowedConfigTokensForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config_token.settings'];
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormId() {
    return 'allowed_config_tokens';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get available filter formats.
    $filter_formats = [];
    $formats = filter_formats();
    foreach ($formats as $format) {
      if ($format->status()) {
        $filter_formats[$format->id()] = $format->label();
      }
    }
    asort($filter_formats);

    // Get config.
    $config = $this->config('config_token.settings');
    $tokens = (array) $config->get('allowed_tokens');

    // Add empty entry at the end of list.
    $tokens[''] = '';

    // @todo add link to token list /admin/help/token
    $form['values'] = [
      '#type' => 'table',
      '#caption' => '',
      '#header' => [
        $this->t('Format'),
        $this->t('Token (machine name)'),
        $this->t('Name'),
        $this->t('Description'),
        $this->t('Action'),
      ],
    ];

    $i = 0;
    foreach ($tokens as $token => $value) {
      $i++;

      $form['values'][$i]['format_id'] = [
        '#type' => 'select',
        '#options' => $filter_formats,
        '#value' => $value['format_id'] ?? '',
      ];

      $form['values'][$i]['token'] = [
        '#type' => 'textfield',
        '#value' => $token,
      ];

      $form['values'][$i]['name'] = [
        '#type' => 'textfield',
        '#value' => $value['name'] ?? '',
      ];

      $form['values'][$i]['description'] = [
        '#type' => 'textfield',
        '#value' => $value['description'] ?? '',
      ];

      if (!empty($value)) {
        $form['values'][$i]['delete'] = [
          '#type' => 'submit',
          '#value' => $this->t('Delete'),
          '#name' => 'delete',
          '#index' => $i,
        ];
      }
      else {
        $form['values'][$i]['add'] = [
          '#type' => 'submit',
          '#value' => $this->t('Add'),
          '#name' => 'add',
          '#index' => $i,
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('config_token.settings');
    $trigger = $form_state->getTriggeringElement();
    $values = $form_state->getValue('values');
    $tokens = [];
    foreach ($values as $key => $item) {
      // Button delete was used.
      if ($trigger['#name'] === 'delete' && $key === $trigger['#index']) {
        continue;
      }
      $tokens[$item['token']] = [
        'name' => $item['name'],
        'description' => $item['description'],
        'format_id' => $item['format_id'],
      ];
    }
    $config->set('allowed_tokens', $tokens);
    $config->save();
    parent::submitForm($form, $form_state);

    // Invalidate the token cache.
    token_clear_cache();
  }

}
