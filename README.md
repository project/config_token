# Config Token

Allows custom tokens to be stored and exported via configuration manager.

Supported modules:
* Domain
* Token Filter

## Similar modules

* Token Custom: The difference is the storage. Token custom has a custom table
 to store configuration, which means, if we want to deploy custom tokens
 programmatically, we need to insert rows into the table, whereas Config Token
 allows you to export and import using Configuration manager.

## Usage

When you install the module, it will create some example tokens, that you can
see under /admin/help/token.

To configure allowed tokens and custom tokens,
visit /admin/config/system/config_tokens

### Displaying tokens
You can use the tokens on fields that are processed by token filters i.e. Body

`[config_token:example_email]`

## Working with Token filter module
* Enable token filter (Replaces global and entity tokens with their values)
  on admin/config/content/formats
* Create a new content and put the token on the Body field
  i.e. [config_token:example_link]

## Overriding config tokens
Since config tokens are configs, it is possible to override them via
settings.php using https://www.drupal.org/project/config_override

## Working with Domain module

All you need to do to override the custom token configuration per domain is to
create a config file with this pattern:

`domain.config.[domain alias].config_token.tokens.yml`

### TODO

* Alter Token cache context to add url.site
