<?php

namespace Drupal\Tests\config_token\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Config Token base class.
 *
 * @group config_token
 *
 * Class ConfigTokenBase
 * @package Drupal\Tests\config_token\Functional
 */
abstract class ConfigTokenBase extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['token', 'config_token', 'token_filter'];

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create admin user.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer filters',
    ], 'Config Token Admin', TRUE);
    $this->drupalLogin($this->adminUser);

    // Configure plain text format.
    $this->drupalGet('admin/config/content/formats/manage/plain_text');
    $this->submitForm([
      'filters[filter_autop][status]' => FALSE,
      'filters[filter_url][status]' => FALSE,
    ], $this->t('Save configuration'));

    $this->drupalGet('admin/config/content/formats/manage/basic_html');
    $this->submitForm([
      'filters[filter_url][status]' => TRUE,
      'filters[token_filter][status]' => TRUE,
    ], $this->t('Save configuration'));
  }

}
