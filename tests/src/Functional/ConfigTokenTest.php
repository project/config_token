<?php

namespace Drupal\Tests\config_token\Functional;

/**
 * Config Token tests.
 *
 * @group config_token
 *
 * Class ConfigTokenTest
 * @package Drupal\Tests\config_token\Functional
 */
class ConfigTokenTest extends ConfigTokenBase {

  /**
   * Token replacements.
   */
  public function testTokens() {
    $value = \Drupal::token()->replace('[config_token:example_email]', [], ['clear' => FALSE]);
    $this->assertEquals($value, 'email@example.com');

    $value = \Drupal::token()->replace('[config_token:example_phone]', [], ['clear' => FALSE]);
    $this->assertEquals($value, '02070000000');

    $value = \Drupal::token()->replace('[config_token:example_link]', [], ['clear' => FALSE]);
    $this->assertEquals($value, '<a href="http://www.example.com">http://www.example.com</a>');
  }

}
