<?php

namespace Drupal\Tests\config_token\Functional;

/**
 * Tests Config Token admin ui.
 *
 * @group config_token
 *
 * Class ConfigTokenAdminUi
 * @package Drupal\Tests\config_token\Functional
 */
class ConfigTokenAdminUi extends ConfigTokenBase {

  /**
   * Test Admin UI.
   */
  public function testAdminUi() {
    $this->drupalLogin($this->adminUser);

    // Go to configuration form.
    $this->drupalGet('/admin/config/system/config_tokens/allowed_tokens');
    // Check links.
    $this->assertSession()->linkExists('Config tokens');
    $this->assertSession()->linkExists('Allowed Config tokens');
    // Check values.
    $this->assertSession()->fieldValueEquals('values[1][format_id]', 'plain_text');
    $this->assertSession()->fieldValueEquals('values[1][token]', 'example_email');
    $this->assertSession()->fieldValueEquals('values[1][name]', 'Example email');
    $this->assertSession()->fieldValueEquals('values[1][description]', 'This is an example of custom token');

    // Test add.
    $edit = [
      'values[4][format_id]' => 'plain_text',
      'values[4][token]' => 'foo',
      'values[4][name]' => 'Foo name',
      'values[4][description]' => 'Foo description',
    ];
    $this->submitForm($edit, 'Add');
    $this->assertSession()->fieldValueEquals('values[4][description]', 'Foo description');

    // Add a second one to test deleting.
    $edit = [
      'values[5][format_id]' => 'plain_text',
      'values[5][token]' => 'bar',
      'values[5][name]' => 'Bar name',
      'values[5][description]' => 'Bar description',
    ];
    $this->submitForm($edit, 'Add');
    $this->assertSession()->fieldValueEquals('values[5][description]', 'Bar description');

    // Test delete.
    $this->submitForm([], 'Delete');
    $this->assertSession()->fieldValueEquals('values[5][description]', '');

    // Test update.
    $edit = [
      'values[4][description]' => 'Foo description',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->fieldValueEquals('values[4][description]', 'Foo description');

    // Go to Config tokens form.
    $this->drupalGet('/admin/config/system/config_tokens');
    $this->assertSession()->linkExists('Config tokens');
    $this->assertSession()->linkExists('Allowed Config tokens');

    // Test add.
    $edit = [
      'values[4][token]' => 'foo',
      'values[4][value]' => 'bar',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->fieldValueEquals('values[4][value]', 'bar');

    // Test update.
    $edit = [
      'values[4][value]' => 'Bar',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->fieldValueEquals('values[4][value]', 'Bar');

    // Check tokens from config.
    $this->assertSession()->pageTextContains('[config_token:example_email]');
    $this->assertSession()->pageTextContains('[config_token:example_phone]');
    $this->assertSession()->pageTextContains('[config_token:example_link]');
    $this->assertSession()->fieldValueEquals('values[1][value]', 'email@example.com');
    $this->assertSession()->fieldValueEquals('values[2][value]', '02070000000');
    $this->assertSession()->fieldValueEquals('values[3][value]', 'http://www.example.com');
    $this->assertSession()->fieldValueEquals('values[4][token]', 'foo');
    $this->assertSession()->fieldValueEquals('values[4][value]', 'Bar');

    // Test token replacements.
    $value = \Drupal::token()->replace('[config_token:foo]', [], ['clear' => FALSE]);
    $this->assertEquals($value, 'Bar');

    // Test delete.
    $this->submitForm([], 'Delete');
    $this->assertSession()->pageTextContains('[config_token:example_email]');
    $this->assertSession()->pageTextContains('[config_token:example_phone]');
    $this->assertSession()->pageTextContains('[config_token:example_link]');
    $this->assertSession()->pageTextNotContains('[config_token:bar]');
  }

}
