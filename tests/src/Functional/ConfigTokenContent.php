<?php

namespace Drupal\Tests\config_token\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Tests Config Tokens on content.
 *
 * @group config_token
 *
 * Class ConfigTokenContent
 * @package Drupal\Tests\config_token\Functional
 */
class ConfigTokenContent extends ConfigTokenBase {

  use StringTranslationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['token', 'node', 'field', 'text',
    'config_token', 'token_filter',
  ];

  /**
   * Token replacements with Token filter.
   */
  public function testTokenFilter() {
    $this->drupalGet('node/add/page');
    $this->submitForm([
      'title[0][value]' => $this->randomMachineName(),
      'body[0][value]' => 'Email:[config_token:example_email] Phone:[config_token:example_phone] Link:[config_token:example_link]',
      'body[0][format]' => 'basic_html',
    ], $this->t('Save'));

    $this->assertSession()->responseContains('Email:email@example.com Phone:02070000000 Link:<a href="http://www.example.com">http://www.example.com</a>');
  }

}
